package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;

public class Robot extends TimedRobot {

  public static final int FRONT_LEFT = 0;
  public static final int FRONT_RIGHT = 1;
  public static final int BACK_LEFT = 2;
  public static final int BACK_RIGHT = 3;

  public static final int CONTROLLER_PORT = 0;

  @Override
  public void robotInit() {
  }

  @Override
  public void autonomousInit() {
  }

  @Override
  public void autonomousPeriodic() {
  }

  @Override
  public void teleopInit() {
  }

  @Override
  public void teleopPeriodic() {
  }

  @Override
  public void testInit() {

  }

  @Override
  public void testPeriodic() {

  }

}
