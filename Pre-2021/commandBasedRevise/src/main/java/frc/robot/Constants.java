/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */

// TODO: Alphabetize sub-classes
public final class Constants {
    public class DriveConstants {
        // Logging
        public static final boolean kLoggingEnabled = true;
        
        // CANBus
        public static final int kFrontLeftMotorID = 2;
        public static final int kFrontRightMotorID = 3;
        public static final int kBackLeftMotorID = 1;
        public static final int kBackRightMotorID = 4;

        // Booleans to change the direction the motors turn
        public static final boolean kLeftMotorsInverted = true; // guess robot is backwards
        public static final boolean kRightMotorsInverted = true;

        public static final boolean kIsLeftVoltageInverted = true; // mainly for autonomous
        public static final boolean kIsRightVoltageInverted = false;
            
        public static final boolean kLeftEncoderReversed = true;
        public static final boolean kRightEncoderReversed = false;

        // Gyro...?
        public static final boolean kGyroReversed = false;

        // Measured in inches
        public static final double kTrackWidth = 24.25;
        public static final double kWheelRadius = 3.5;

        public static final double kGearRatio = 10.75;
        public static final double kEncoderResolution = 2048;

        // Velocity
        public static final double kP = 0.0109;
        public static final double kI = 0.0;
        public static final double kD = 0.0;

        public static final double kS = 0.226;
        public static final double kV = 1.95;
        public static final double kA = 0.299;

        public static final double kRamseteB = 2;
        public static final double kRamseteZeta = 0.7;
    }

    public class IntakeConstants {
        public static final int kIntakeID = 12;
        
        public static final double kIntakeSpeedIn = 0.6;
        public static final double kIntakeSpeedOut = -0.6;
    }

    public class ConveyorConstants { // NOT to be confused with the INDEXER
        public static final int kConveyorID = 7;

        public static final double kConveyorSpeedIn = 0.6;
        public static final double kConveyorSpeedOut = -0.6;
    }

    public class IndexerConstants {
        public static final int kIndexerID = 9;

        public static final double kIndexerSpeedForward = 0.9;
        public static final double kIndexerSpeedBackwards = -0.3; // If copy-pasting from old code, check about negatives

        public static final int kBallCapacity = 5; // Didn't really use this in code, but it might be useful to change in some cases
    }

    public class TOFSensorConstants {
        public static final int kSensorInID = 13;
        public static final int kSensorOutID = 14;

        public static final int kSensorRate = 24;
        public static final double kSensorInThreshold = 9.0;
        public static final double kSenorOutThreshold = 2.5;
        public static final double kMMPerInch = 25.4; // Some weird conversion thingy?
    }

    public class ShooterConstants {
        public static final int kShooterID = 8;
        
        // One of these needs to be negative...?
        public static final double kSpeedIn = 1.0; // Emergency reversed speed
        public static final double kSpeedOut = 0.75; // 0.65
    }

    public class ClimberConstants {
        public static final int kClimberMasterID = 5;
        public static final int kClimberSlaveID = 6;

        public static final double kClimberSpeed = 0.7;
        public static final double kReversedClimberSpeed = -0.7;
    }

    public class BlinkinConstants {
        public static final int kBlinkinPort = 0;

        public static final double kBlue = 0.79;
        public static final double kGold = 0.67;
        public static final double kRed = 0.61;
    }

    public class CameraConstants {
        public static final int kFrontCameraPort = 0;
        public static final int kBackCameraPort = 1; // not actually used
    }

    public class ControllerConstants {
        public static final int kControllerPort = 0;
        public static final int kLeftStickXAxis = 0;
        public static final int kLeftStickYAxis = 1;
        public static final int kRightStickXAxis = 4;
        public static final int kRightStickYAxis = 5;
        public static final double kDeadZoneValue = 0.3;
    }

    public class LimelightConstants { // Honestly I couldn't tell you how to use my function after several months or what these numbers mean.
        public static final int kDriverPipeline = 3;
        public static final int kAutoShooterPipeline = 1;

        // Anything being passed to NT should be a DOUBLE.
        public static final double kXThreshold = 2.0;
        public static final double kAreaThreshold = .2;
        public static final double kAreaAddition = .8; // What does this even mean? I have no clue.
        public static final double kMovementSpeed = .5; // robot go vroom
        public static final double kCenteringSpeed = .5;
    }

    public class JankyInteralTimerConstants { // This is perhaps the jankiest part of the entire robot. Please use PID.
        public static final double kWarmupTime = 0.75;
        public static final double kIndexerTime = 110;
    }
}
