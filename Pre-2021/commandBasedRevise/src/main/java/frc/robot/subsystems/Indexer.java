/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import static frc.robot.Constants.IndexerConstants.*;
import static frc.robot.Constants.TOFSensorConstants.*;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.playingwithfusion.TimeOfFlight;

public class Indexer extends SubsystemBase {
  /**
   * Creates a new Indexer.
   */
  private WPI_TalonSRX indexer;
  
  private TimeOfFlight indexSensorIn;
  private TimeOfFlight indexSensorOut;

  public Indexer() {
    indexer = new WPI_TalonSRX(kIndexerID);
    indexSensorIn = new TimeOfFlight(kSensorInID);
    indexSensorOut = new TimeOfFlight(kSensorOutID);
  }

  public void setIndexerSpeed(double speed) {
    indexer.set(speed);
  }

  public boolean isEntranceBlocked() {
    return true;
  }

  public boolean isExitBlocked() {
    return true;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
