/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

// TODO: finish outputLimelightData method
// TODO: add some getters for various entries
// TODO: add whatever those janky commands were into commands
// TODO: check about getDouble... Some of the network entries are arrays I think
// TODO: how does one do non-static nested classes in order to organize code?

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

public class Limelight extends SubsystemBase {
  /**
   * Creates a new Limelight.
   */
  NetworkTable limelightNetworkTable;
  NetworkTableInstance netInst;

  NetworkTableEntry targetStateEntry;
  NetworkTableEntry targetXOffsetEntry;
  NetworkTableEntry targetYOffsetEntry;
  NetworkTableEntry targetAreaEntry;
  NetworkTableEntry targetSkew;
  NetworkTableEntry pipelineLatency;
  NetworkTableEntry targetShortestSide;
  NetworkTableEntry targetLongestSide;
  NetworkTableEntry targetHorizontalEntry;
  NetworkTableEntry targetVerticalEntry;
  NetworkTableEntry pipelineEntry; 
  NetworkTableEntry ledModeEntry;
  NetworkTableEntry cameraModeEntry;

  public Limelight() {
    limelightNetworkTable = NetworkTableInstance.getDefault().getTable("limelight");
    targetStateEntry = limelightNetworkTable.getEntry("tv");
    targetXOffsetEntry = limelightNetworkTable.getEntry("tx");
    targetYOffsetEntry = limelightNetworkTable.getEntry("ty");
    targetAreaEntry = limelightNetworkTable.getEntry("ta");
    targetSkew = limelightNetworkTable.getEntry("ts");
    pipelineLatency = limelightNetworkTable.getEntry("tl"); // see docs for background on latency
    targetShortestSide = limelightNetworkTable.getEntry("tshort");
    targetLongestSide = limelightNetworkTable.getEntry("tlong");
    targetHorizontalEntry = limelightNetworkTable.getEntry("thor");
    targetVerticalEntry = limelightNetworkTable.getEntry("tvert");
    pipelineEntry = limelightNetworkTable.getEntry("pipeline"); // pipeline vs getPipe
    ledModeEntry = limelightNetworkTable.getEntry("ledMode");
    cameraModeEntry = limelightNetworkTable.getEntry("camMode");
    // currently excluding something called camtran... future complex code?
  }

  public boolean isValidTarget() {
    return targetStateEntry.getDouble(0) == 1 ? true : false;
  }

  public double getXOffset() {
    return targetXOffsetEntry.getDouble(0); // should we have a default value to check if stuff broke? (other than 0)
  }

  public double getYOffset() {
    return targetYOffsetEntry.getDouble(0);
  }

  public double getTargetArea() {
    return targetAreaEntry.getDouble(0);
  }

  public double getTargetSkew() {
    return targetSkew.getDouble(0);
  }

  public double getPipelineLatency() {
    return pipelineLatency.getDouble(0);
  }

  public double getPipeline() {
    return pipelineEntry.getDouble(0);
  }

  public double getLedState() {
    return ledModeEntry.getDouble(0);
  }

  public double getCameraMode() {
    return cameraModeEntry.getDouble(0);
  }

  public double getShortestSide() {
    return targetShortestSide.getDouble(0);
  }
    
  public double getLongestSide() {
    return targetLongestSide.getDouble(0);
  }

  public double getHorizontalLength() {
    return targetHorizontalEntry.getDouble(0);
  }

  public double getVerticalLength() {
    return targetVerticalEntry.getDouble(0);
  }

  public void setPipeline(double pipeline) {
    pipelineEntry.setNumber(pipeline);
  }

  public void setLedState(double state) {
    ledModeEntry.setNumber(state);
  }

  private void outputLimelightData() {
    System.out.println("WARNING! ====BEGIN LIMELIGHT LOG====");
    System.out.println("WARNING! isValidTarget(): " + isValidTarget());
    System.out.println("WARNING! getXOffset(): " + getXOffset());
    System.out.println("WARNING! getYOffset(): " + getYOffset());
    System.out.println("WARNING! getTargetArea(): " + getTargetArea());
    System.out.println("WARNING! getTargetSkew(): " + getTargetSkew());
    System.out.println("WARNING! getPipelineLatency(): " + getPipelineLatency());
    System.out.println("WARNING! getPipeline(): " + getPipeline());
    System.out.println("WARNING! getLedState(): " + getLedState());
    System.out.println("WARNING! getCameraMode(): " + getCameraMode());
    System.out.println("WARNING! getShortestSide(): " + getShortestSide());
    System.out.println("WARNING! getLongestSide(): " + getLongestSide());
    System.out.println("WARNING! getHorizontalLength(): " + getHorizontalLength());
    System.out.println("WARNING! getVerticalLength(): " + getVerticalLength());
  }

  @Override
  public void periodic() {
    outputLimelightData();
  }
}
