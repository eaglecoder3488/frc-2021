/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import static frc.robot.Constants.CameraConstants.*;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.cameraserver.CameraServer;

public class Camera extends SubsystemBase {
  /**
   * Creates a new Camera.
   */
  private UsbCamera camera;

  public Camera() {
    camera = CameraServer.getInstance().startAutomaticCapture(kFrontCameraPort);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
