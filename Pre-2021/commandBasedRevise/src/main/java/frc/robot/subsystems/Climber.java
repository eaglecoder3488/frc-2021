/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;
import static frc.robot.Constants.ClimberConstants.*;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Climber extends SubsystemBase {
  /**
   * Creates a new Climber.
   */
  private WPI_VictorSPX climberMaster;
  private WPI_VictorSPX climberSlave;

  public Climber() {
    climberMaster = new WPI_VictorSPX(kClimberMasterID);
    climberSlave = new WPI_VictorSPX(kClimberSlaveID);

    // set slave to follow master controller
    climberSlave.follow(climberMaster);
  }

  public void setClimberSpeed(double speed) {
    climberMaster.set(speed);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
