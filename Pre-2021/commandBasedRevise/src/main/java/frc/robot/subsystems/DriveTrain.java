/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

// By statically importing, we can reference variables directly as if they were defined in this class
import static frc.robot.Constants.DriveConstants.*;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

// We have some issues with the Falcons... Useful thread? Sounds like our issues.
// https://www.chiefdelphi.com/t/falcon-500-follow-voltage-issues/372875/10

// Notable change in case this is forgetten in the future... Encoders have been reversed in code.
// No need to change them hardware-wise or via the library, simply used the ? : thing instead.
// TODO: check if everything is reset before odometry is created
// TODO: obtain average values of encoders, rather than just taking the values of the ones from the front
// TODO: improved logging?

public class DriveTrain extends SubsystemBase {
  /**
   * Creates a new DriveTrain.
   */
  private WPI_TalonFX frontLeftMotor, frontRightMotor, backLeftMotor, backRightMotor;
  private SpeedControllerGroup leftMotorGroup, rightMotorGroup;

  private AHRS gyro;

  private DifferentialDrive driveTrain;
  private DifferentialDriveKinematics kinematics;
  private DifferentialDriveOdometry odometry;
  private SimpleMotorFeedforward feedForward;
  private PIDController leftPIDController;
  private PIDController rightPIDController;

  private NetworkTable networkTable;

  public DriveTrain() {
    frontLeftMotor = new WPI_TalonFX(kFrontLeftMotorID);
    frontRightMotor = new WPI_TalonFX(kFrontRightMotorID);
    backLeftMotor = new WPI_TalonFX(kBackLeftMotorID);
    backRightMotor = new WPI_TalonFX(kBackRightMotorID);

    setDriveMode(NeutralMode.Brake);

    leftMotorGroup = new SpeedControllerGroup(frontLeftMotor, backLeftMotor);
    rightMotorGroup = new SpeedControllerGroup(frontRightMotor, backRightMotor);

    driveTrain = new DifferentialDrive(leftMotorGroup, rightMotorGroup);

    gyro = new AHRS(SPI.Port.kMXP);

    resetEncoders(); // make sure odometry isn't screwed up

    kinematics = new DifferentialDriveKinematics(Units.inchesToMeters(kTrackWidth));
    odometry = new DifferentialDriveOdometry(getHeading());
    feedForward = new SimpleMotorFeedforward(kS, kP, kA);
    leftPIDController = new PIDController(kP, kI, kD);
    rightPIDController = new PIDController(kP, kI, kD);

    if(kLoggingEnabled) {
      networkTable = NetworkTableInstance.getDefault().getTable("driveTelementary");
    }
  }

  @Override
  public void periodic() {
    odometry.update(getHeading(), getLeftWheelDistance(), getRightWheelDistance());
    if(kLoggingEnabled) {
      outputTelementary();
    }
  }

  // Directly controlling DriveTrain
  public void setTankDriveVolts(double leftVolts, double rightVolts) {
    leftMotorGroup.setVoltage((kIsLeftVoltageInverted) ? -leftVolts : leftVolts);
    rightMotorGroup.setVoltage((kIsRightVoltageInverted) ? -rightVolts : rightVolts);
  }

  public void setMaxOutput(double maxOutput) {
    driveTrain.setMaxOutput(maxOutput);
  }

  public void setDriveMode(NeutralMode driveMode) {
    frontLeftMotor.setNeutralMode(driveMode);
    frontRightMotor.setNeutralMode(driveMode);
    backLeftMotor.setNeutralMode(driveMode);
    backRightMotor.setNeutralMode(driveMode);
  }

  public void arcadeDrive(double fwd, double rot) {
    driveTrain.arcadeDrive(fwd, rot);
  }

  // Obtaining DriveTrain Information
  public DifferentialDriveWheelSpeeds getDriveWheelSpeeds() {
    // returns wheel speeds in m/s (meters per second)
    //double leftVelocity = Units.inchesToMeters( getLeftEncoderVelocity() / PULSE_PER_INCH );
    //double rightVelocity = Units.inchesToMeters( getLeftEncoderVelocity() / PULSE_PER_INCH );
    double leftRotationsPerSecond = (double) getLeftEncoderVelocity() / kEncoderResolution / kGearRatio * 10;        
    double leftVelocity = leftRotationsPerSecond * 2 * Math.PI * Units.inchesToMeters(kWheelRadius);
    double rightRotationsPerSecond = (double) getRightEncoderVelocity() / kEncoderResolution / kGearRatio * 10;        
    double rightVelocity = rightRotationsPerSecond * 2 * Math.PI * Units.inchesToMeters(kWheelRadius);
    return new DifferentialDriveWheelSpeeds(leftVelocity, rightVelocity);
  }

  public Rotation2d getHeading() {
    return Rotation2d.fromDegrees((kGyroReversed) ? -gyro.getAngle() : gyro.getAngle());
  }

  public double getLeftWheelDistance() {
    double leftDistance = ((double) getLeftEncoderPosition()) / kEncoderResolution / kGearRatio * 2 * Math.PI * Units.inchesToMeters(kWheelRadius);
    return leftDistance;
  }

  public double getRightWheelDistance() {
    double rightDistance = ((double) getRightEncoderPosition()) / kEncoderResolution / kGearRatio * 2 * Math.PI * Units.inchesToMeters(kWheelRadius);
    return rightDistance;
  }

  public int getLeftEncoderPosition() {
    return (kLeftEncoderReversed) ? -frontLeftMotor.getSelectedSensorPosition() : frontLeftMotor.getSelectedSensorPosition();
  }

  public int getRightEncoderPosition() {
    return (kRightEncoderReversed) ? -frontRightMotor.getSelectedSensorPosition() : frontRightMotor.getSelectedSensorPosition();
  }

  public int getLeftEncoderVelocity() {
    return (kLeftEncoderReversed) ? -frontLeftMotor.getSelectedSensorVelocity() : frontLeftMotor.getSelectedSensorVelocity();
  }

  public int getRightEncoderVelocity() {
    return (kRightEncoderReversed) ? -frontRightMotor.getSelectedSensorVelocity() : frontRightMotor.getSelectedSensorVelocity();
  }

  public DifferentialDriveKinematics getKinematics() {
    return kinematics;
  }

  public DifferentialDriveOdometry getOdometry() {
    return odometry;
  }

  public SimpleMotorFeedforward getSimpleMotorFeedForward() {
    return feedForward;
  }

  public PIDController getLeftPIDController() {
    return leftPIDController;
  }

  public PIDController getRightPIDController() {
    return rightPIDController;
  }

  public Pose2d getPose() {
    return odometry.getPoseMeters();
  }

  public double getGyroAngle() {
    return gyro.getAngle();
  }

  // Reset Functions
  public void resetOdometry(Pose2d pose) {
    resetEncoders();
    resetGyro();
    odometry.resetPosition(pose, getHeading());
  }

  public void resetEncoders() {
    frontLeftMotor.setSelectedSensorPosition(0);
    frontRightMotor.setSelectedSensorPosition(0);
  }

  public void resetGyro() {
    gyro.reset();
  }

  public void outputTelementary() {
    // note that odometry is in meters and such
    double gyroAngle = (kGyroReversed) ? -gyro.getAngle() : gyro.getAngle();
    double leftSideRotationsPerSecond = (double) getLeftEncoderVelocity() / kEncoderResolution / kGearRatio * 10;
    double leftSidePhysicalVelocity = leftSideRotationsPerSecond * 2 * Math.PI * Units.inchesToMeters(kWheelRadius);
    double leftSideDistanceTraversed = ((double) getLeftEncoderPosition()) / kEncoderResolution / kGearRatio * 2 * Math.PI * Units.inchesToMeters(kWheelRadius);
    double rawLeftEncoderPosition = (kLeftEncoderReversed) ? -frontLeftMotor.getSelectedSensorPosition() : frontLeftMotor.getSelectedSensorPosition();
    double rawLeftEncoderVelocity = (kLeftEncoderReversed) ? -frontLeftMotor.getSelectedSensorVelocity() : frontLeftMotor.getSelectedSensorVelocity();
    double frontLeftMotorInputCurrent = frontLeftMotor.getSupplyCurrent();
    double frontLeftMotorOutputCurrent = frontLeftMotor.getStatorCurrent();
    double frontLeftMotorOutputVoltage = frontLeftMotor.getMotorOutputVoltage();
    double frontLeftMotorOutputPercent = frontLeftMotor.getMotorOutputPercent();
    double backLeftMotorInputCurrent = backLeftMotor.getSupplyCurrent();
    double backLeftMotorOutputCurrent = backLeftMotor.getStatorCurrent();
    double backLeftMotorOutputVoltage = backLeftMotor.getMotorOutputVoltage();
    double backLeftMotorOutputPercent = backLeftMotor.getMotorOutputPercent();
    double rightSideRotationsPerSecond = (double) getRightEncoderVelocity() / kEncoderResolution / kGearRatio * 10;
    double rightSidePhysicalVelocity = rightSideRotationsPerSecond * 2 * Math.PI * Units.inchesToMeters(kWheelRadius);
    double rightSideDistanceTraversed = ((double) getRightEncoderPosition()) / kEncoderResolution / kGearRatio * 2 * Math.PI * Units.inchesToMeters(kWheelRadius);
    double rawRightEncoderPosition = (kRightEncoderReversed) ? -frontRightMotor.getSelectedSensorPosition() : frontRightMotor.getSelectedSensorPosition();
    double rawRightEncoderVelocity = (kRightEncoderReversed) ? -frontRightMotor.getSelectedSensorVelocity() : frontRightMotor.getSelectedSensorVelocity();
    double frontRightMotorInputCurrent = frontRightMotor.getSupplyCurrent();
    double frontRightMotorOutputCurrent = frontRightMotor.getStatorCurrent();
    double frontRightMotorOutputVoltage = frontRightMotor.getMotorOutputVoltage();
    double frontRightMotorOutputPercent = frontRightMotor.getMotorOutputPercent();
    double backRightMotorInputCurrent = backRightMotor.getSupplyCurrent();
    double backRightMotorOutputCurrent = backRightMotor.getStatorCurrent();
    double backRightMotorOutputVoltage = backRightMotor.getMotorOutputVoltage();
    double backRightMotorOutputPercent = backRightMotor.getMotorOutputPercent();

    // Sending data to NetworkTables to be (hopefully) read by dashboard
    networkTable.getEntry("gyroAngle").setDouble(gyroAngle);
    networkTable.getEntry("leftSideRotationsPerSecond").setDouble(leftSideRotationsPerSecond);
    networkTable.getEntry("leftSidePhysicalVelocity").setDouble(leftSidePhysicalVelocity);
    networkTable.getEntry("leftSideDistanceTraversed").setDouble(leftSideDistanceTraversed);
    networkTable.getEntry("rawLeftEncoderPosition").setDouble(rawLeftEncoderPosition);
    networkTable.getEntry("rawLeftEncoderVelocity").setDouble(rawLeftEncoderVelocity);
    networkTable.getEntry("frontLeftMotorInputCurrent").setDouble(frontLeftMotorInputCurrent);
    networkTable.getEntry("frontLeftMotorOutputCurrent").setDouble(frontLeftMotorOutputCurrent);
    networkTable.getEntry("frontLeftMotorOutputVoltage").setDouble(frontLeftMotorOutputVoltage);
    networkTable.getEntry("frontLeftMotorOutputPercent").setDouble(frontLeftMotorOutputPercent);
    networkTable.getEntry("backLeftMotorInputCurrent").setDouble(backLeftMotorInputCurrent);
    networkTable.getEntry("backLeftMotorOutputCurrent").setDouble(backLeftMotorOutputCurrent);
    networkTable.getEntry("backLeftMotorOutputVoltage").setDouble(backLeftMotorOutputVoltage);
    networkTable.getEntry("backLeftMotorOutputPercent").setDouble(backLeftMotorOutputPercent);
    networkTable.getEntry("rightSideRotationsPerSecond").setDouble(rightSideRotationsPerSecond);
    networkTable.getEntry("rightSidePhysicalVelocity").setDouble(rightSidePhysicalVelocity);
    networkTable.getEntry("rightSideDistanceTraversed").setDouble(rightSideDistanceTraversed);
    networkTable.getEntry("rawRightEncoderPosition").setDouble(rawRightEncoderPosition);
    networkTable.getEntry("rawRightEncoderVelocity").setDouble(rawRightEncoderVelocity);
    networkTable.getEntry("frontRightMotorInputCurrent").setDouble(frontRightMotorInputCurrent);
    networkTable.getEntry("frontRightMotorOutputCurrent").setDouble(frontRightMotorOutputCurrent);
    networkTable.getEntry("frontRightMotorOutputPercent").setDouble(frontRightMotorOutputPercent);
    networkTable.getEntry("backRightMotorInputCurrent").setDouble(backRightMotorInputCurrent);
    networkTable.getEntry("backRightMotorOutputCurrent").setDouble(backRightMotorOutputCurrent);
    networkTable.getEntry("backRightMotorOutputVoltage").setDouble(backRightMotorOutputVoltage);
    networkTable.getEntry("backRightMotorOutputPercent").setDouble(backRightMotorOutputPercent);

    // Verbose logging (the WARNING! allows for info to be seen in driver station)
    System.out.println("WARNING! ====BEGIN DRIVETRAIN LOG====");
    System.out.println("WARNING! gyroAngle: " + gyroAngle);
    System.out.println("WARNING! <<<<<< LEFT TRAIN <<<<<<");
    System.out.println("WARNING! leftSideRotationsPerSecond: " + leftSideRotationsPerSecond);
    System.out.println("WARNING! leftSidePhysicalVelocity: " + leftSidePhysicalVelocity);
    System.out.println("WARNING! leftSideDistanceTraversed: " + leftSideDistanceTraversed);
    System.out.println("WARNING! rawLeftEncoderPosition: " + rawLeftEncoderPosition);
    System.out.println("WARNING! rawLeftEncoderVelocity: " + rawLeftEncoderVelocity);
    System.out.println("WARNING! frontLeftMotorInputCurrent: " + frontLeftMotorInputCurrent);
    System.out.println("WARNING! frontLeftMotorOutputCurrent: " + frontLeftMotorOutputCurrent);
    System.out.println("WARNING! frontLeftMotorOutputVoltage: " + frontLeftMotorOutputVoltage);
    System.out.println("WARNING! frontLeftMotorOutputPercent: " + frontLeftMotorOutputPercent);
    System.out.println("WARNING! backLeftMotorInputCurrent: " + backLeftMotorInputCurrent);
    System.out.println("WARNING! backLeftMotorOutputCurrent: " + backLeftMotorOutputCurrent);
    System.out.println("WARNING! backLeftMotorOutputVoltage: " + backLeftMotorOutputVoltage);
    System.out.println("WARNING! backLeftMotorOutputPercent: " + backLeftMotorOutputPercent);
    System.out.println("WARNING! >>>>>> RIGHT TRAIN >>>>>>");
    System.out.println("WARNING! rightSideRotationsPerSecond: " + rightSideRotationsPerSecond);
    System.out.println("WARNING! rightSidePhysicalVelocity: " + rightSidePhysicalVelocity);
    System.out.println("WARNING! rightSideDistanceTraversed: " + rightSideDistanceTraversed);
    System.out.println("WARNING! rawRightEncoderPosition: " + rawRightEncoderPosition);
    System.out.println("WARNING! rawRightEncoderVelocity: " + rawRightEncoderVelocity);
    System.out.println("WARNING! frontRightMotorInputCurrent: " + frontRightMotorInputCurrent);
    System.out.println("WARNING! frontRightMotorOutputCurrent: " + frontRightMotorOutputCurrent);
    System.out.println("WARNING! frontRightMotorOutputVoltage: " + frontRightMotorOutputVoltage);
    System.out.println("WARNING! frontRightMotorOutputPercent: " + frontRightMotorOutputPercent);
    System.out.println("WARNING! backRightMotorInputCurrent: " + backRightMotorInputCurrent);
    System.out.println("WARNING! backRightMotorOutputCurrent: " + backRightMotorOutputCurrent);
    System.out.println("WARNING! backRightMotorOutputVoltage: " + backRightMotorOutputVoltage);
    System.out.println("WARNING! backRightMotorOutputPercent: " + backRightMotorOutputPercent);
  }
}
