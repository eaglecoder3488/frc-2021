/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import static frc.robot.Constants.BlinkinConstants.*;

public class Blinkin extends SubsystemBase {
  /**
   * Creates a new Blinkin.
   */
  private Spark blinkin;

  public Blinkin() {
    blinkin = new Spark(kBlinkinPort);
  }

  public void setColor(double color) {
    blinkin.set(color);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
