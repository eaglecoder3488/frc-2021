package frc.robot;

import static frc.robot.Constants.ControllerConstants.*;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;

public class Controller {
    private XboxController xboxPad;

    public Controller() {
        xboxPad = new XboxController(kControllerPort);
    }

    // Can we clean this up a bit? Code is a tad bit repeatative
    public double getLeftStickX(boolean isDeadzoneApplied) {
        double rawAxis = xboxPad.getRawAxis(kLeftStickXAxis);
        return isDeadzoneApplied ? applyDeadzone(rawAxis) : rawAxis;
    }

    public double getLeftStickY(boolean isDeadzoneApplied) {
        double rawAxis = xboxPad.getRawAxis(kLeftStickYAxis);
        return isDeadzoneApplied ? applyDeadzone(rawAxis) : rawAxis;
    }

    public double getRightStickX(boolean isDeadzoneApplied) {
        double rawAxis = xboxPad.getRawAxis(kRightStickXAxis);
        return isDeadzoneApplied ? applyDeadzone(rawAxis) : rawAxis;
    }

    public double getRightStickY(boolean isDeadzoneApplied) {
        double rawAxis = xboxPad.getRawAxis(kRightStickYAxis);
        return isDeadzoneApplied ? applyDeadzone(rawAxis) : rawAxis;
    }

    public boolean getAButton() {
        return xboxPad.getAButton();
    }

    public boolean getBButton() {
        return xboxPad.getBButton();
    }

    public boolean getXButton() {
        return xboxPad.getXButton();
    }

    public boolean getYButton() {
        return xboxPad.getYButton();
    }

    public boolean getDPadUp() {
        return xboxPad.getPOV() == 0;
    }

    public boolean getDPadRight() {
        return xboxPad.getPOV() == 90;
    }

    public boolean getDPadDown() {
        return xboxPad.getPOV() == 180;
    }

    public boolean getDPadLeft() {
        return xboxPad.getPOV() == 270;
    }

    public boolean getLeftBumper() {
        return xboxPad.getBumper(Hand.kLeft);
    }
    public boolean getRightBumper() {
        return xboxPad.getBumper(Hand.kRight);
    }

    public double getLeftTrigger() {
        return xboxPad.getTriggerAxis(Hand.kLeft);
    }
    public double getRightTrigger() {
        return xboxPad.getTriggerAxis(Hand.kRight);
    }

    public double getLeftTrigger(boolean isDeadzoneApplied) {
        double rawAxis = xboxPad.getTriggerAxis(Hand.kLeft);
        return isDeadzoneApplied ? applyDeadzone(rawAxis) : rawAxis;
    }

    public double getRightTrigger(boolean isDeadzoneApplied) {
        double rawAxis = xboxPad.getTriggerAxis(Hand.kRight);
        return isDeadzoneApplied ? applyDeadzone(rawAxis) : rawAxis;
    }

    private static double applyDeadzone(double in) {
        if(Math.abs(in) < kDeadZoneValue)
            return 0.0;
        else
            return in;
    }
}