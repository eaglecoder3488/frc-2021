/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.PWMVictorSPX;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import static frc.robot.RobotMap.*;

/**
 * Add your docs here.
 */
public class DriveTrain extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  private PWMVictorSPX leftFront, leftBack, rightFront, rightBack;
  private SpeedControllerGroup leftGroup, rightGroup;
  private DifferentialDrive driveTrain;

  public DriveTrain() {
    leftFront = new PWMVictorSPX(FRONT_LEFT);
    leftBack = new PWMVictorSPX(BACK_LEFT);
    rightFront = new PWMVictorSPX(FRONT_RIGHT);
    rightBack = new PWMVictorSPX(BACK_RIGHT);

    leftGroup = new SpeedControllerGroup(leftFront, leftBack);
    rightGroup = new SpeedControllerGroup(rightFront, rightBack);

    driveTrain = new DifferentialDrive(leftGroup, rightGroup);
  }

  public void arcadeDrive(double fwd, double rot) {
    driveTrain.arcadeDrive(fwd, rot);
  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
  }
}
